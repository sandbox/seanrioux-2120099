
(function ($) {
  Drupal.color = {
    logoChanged: false,
    callback: function(context, settings, form, farb, height, width) {
      // Change the logo to be the real one.
      if (!this.logoChanged) {
        $('#preview #preview-logo img').attr('src', Drupal.settings.color.logo);
        this.logoChanged = true;
      }
      // Remove the logo if the setting is toggled off. 
      if (Drupal.settings.color.logo == null) {
        $('div').remove('#preview-logo');
      }

      // Text preview.
      $('#preview #preview-content', form).css('color', $('#palette input[name="palette[content_text]"]', form).val());
       $('#preview #preview-content h4', form).css('color', $('#palette input[name="palette[content_text_h]"]', form).val());
	   $('#preview #preview-content a', form).css('color', $('#palette input[name="palette[link]"]', form).val());
	   $('#preview #preview-content a:hover', form).css('color', $('#palette input[name="palette[link_hover]"]', form).val());

	  // Buttons.
	  
	   $('#preview #preview-button, #preview #preview-taskbar', form).css('background', $('#palette input[name="palette[taskbar_back]"]', form).val());
	   $('#preview #preview-button-hover', form).css('background', $('#palette input[name="palette[button_hover]"]', form).val());
	   
	    // header wrapper background.
      $('#preview #preview-header', form).css('background', $('#palette input[name="palette[header_back]"]', form).val());
		

      // first wrapper background.
      $('#preview #preview-first', form).css('background', $('#palette input[name="palette[first_back]"]', form).val());
	  
	   // diptych wrapper background.
      $('#preview #preview-diptych', form).css('background', $('#palette input[name="palette[diptych_back]"]', form).val());
	  
	 // triptych wrapper background.
      $('#preview #preview-triptych', form).css('background', $('#palette input[name="palette[triptych_back]"]', form).val());
	  
	  // Quadtych wrapper background.
      $('#preview #preview-quadtych', form).css('background', $('#palette input[name="palette[quadtych_back]"]', form).val());
	  
	    // last wrapper background.
      $('#preview #preview-last', form).css('background', $('#palette input[name="palette[last_back]"]', form).val());

    }
  };
})(jQuery);
