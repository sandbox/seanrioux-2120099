<?php

// Put the logo path into JavaScript for the live preview.
drupal_add_js(array('color' => array('logo' => theme_get_setting('logo', 'artwar_editor'))), 'setting');

$info = array(
  'fields' => array(
  // Color areas //
    'taskbar_back'        => t('Taskbar'),
	'content_text'        => t('main section text color'),
	'content_text_h'      => t('main section heading color'),
    'header_back'         => t('Header Background'),
    'first_back'          => t('First Background'),
    'diptych_back'        => t('Diptych Background'),
    'triptych_back'       => t('Triptych Background'),
	'quadtych_back'       => t('Last Background'),
    'link'                => t('Links'),
    'link_hover'          => t('Links Hover'),
  ),
  
  'schemes' => array(
    'default' => array(
	  // default artwar_editor theme //
      'title' => t('Default'),
      'colors' => array(
	'taskbar_back'        => '#464650',
	'content_text'        => '#3e3e47',
	'content_text_h'      => '#4e4e59',
    'header_back'         => '#565662',
    'first_back'          => '#8967ac',
    'diptych_back'        => '#cb5178',
    'triptych_back'       => '#f58235',
	'quadtych_back'       => '#54c2ba',
    'link'                => '#805ca6',
    'link_hover'          => '#9272b2',
      ),
    ),
	'firehouse' => array(
      'title' => t('artwar_editor'),
      'colors' => array(
    'taskbar_back'        => '#464650',
	'content_text'        => '#3e3e47',
	'content_text_h'      => '#4e4e59',
    'header_back'         => '#565662',
    'first_back'          => '#8967ac',
    'diptych_back'        => '#cb5178',
    'triptych_back'       => '#f58235',
	'quadtych_back'       => '#54c2ba',
    'link'                => '#805ca6',
    'link_hover'          => '#9272b2',
      ),
    ),
  ),
  
  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'css/colors.css',
  ),

  // Files to copy.
  'copy' => array(
    'logo.png',
  ),

  // Gradient definitions.
  'gradients' => array(),

  // Color areas to fill (x, y, width, height).
  'fill' => array(),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_css' => 'color/preview.css',
  'preview_js' => 'color/preview.js',
  'preview_html' => 'color/preview.html',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);
